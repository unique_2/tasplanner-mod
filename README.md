TAS-Planner Factorio Mod
=====================

Utility for planning things and stuff.

Features: 
 * calculate research costs, 
 * reset resources and mining drills on a surface (will remove modules from mining drills)
 * create new surfaces with the same map settings, 
 * change between surfaces, 
 * zoom.

To calculate a research cost enter a command like

```lua
/c remote.call("tasplanner", "add", {"rocket-silo", "concrete", "electric-energy-accumulators-1", "solar-energy", "effect-transmission", "fluid-handling", "logistics-2", "automobilism"})
```

To add a new surface enter 

```lua
/c remote.call("tasplanner", "new_surface")
```

All other features are in hotkeys.