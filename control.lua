
require("mod-gui")

-- Utils
function output(str)
	game.print("tech-calc: " .. str)
end

-- Main
function recalculate()
	local technologies = {}
	for name, v in pairs(global.requirements) do
		if v then
			technologies[#technologies + 1] = name
		end
	end

	global.technologies = {}
	global.costs = {}

	while #technologies > 0 do
		local tech = game.technology_prototypes[technologies[1]]
		table.remove(technologies, 1)
		if not global.technologies[tech.name] then
			global.technologies[tech.name] = true
			if tech.research_unit_ingredients  then
				local count = tech.research_unit_count
				for _, ingredient in ipairs(tech.research_unit_ingredients ) do
					if not global.costs[ingredient.name] then global.costs[ingredient.name] = {count=0, time=0} end
					global.costs[ingredient.name].count = global.costs[ingredient.name].count + count * ingredient.amount
					global.costs[ingredient.name].time = global.costs[ingredient.name].time + tech.research_unit_energy * count / 60
				end
			end

			if tech.prerequisites then
				for name, prototype in pairs(tech.prerequisites) do
					technologies[#technologies + 1] = name
				end
			end
		end
	end

	write_techs()
end

function write_techs()
	local s = "All necessary Technologies: "
	for name, v in pairs(global.technologies) do
		if v then
			s = s .. name .. ", "
		end
	end

	game.print(s)

	s = "Original Requirements: "
	for name, v in pairs(global.requirements) do
		if v then
			s = s .. name .. ", "
		end
	end
	game.print(s)
	for name, amount in pairs(global.costs) do
		game.print(name .. ": " .. amount.count .. " in " .. amount.time .. " sec." )
	end
end

function calculate_technologies(techs)
	if type(techs) == type("") then
		techs = {techs}
	end
	global.requirements = {}

	for _, name in pairs(techs) do
		if not game.technology_prototypes[name] then
			output("Tech not found: " .. name)
		elseif global.requirements[name] then
			output("Tech already in requirements: " .. name)
		else
			global.requirements[name] = true
		end
	end
	recalculate()
end

function reset_resources(surface)
	local r = 20
	for _, ent in pairs(surface.find_entities_filtered{area={{-r*32, -r*32}, {r*32, r*32}}, type="resource"}) do
		ent.destroy()
	end
	local entities = {"coal", "copper-ore", "iron-ore", "stone", "crude-oil", "uranium-ore"}
	local chunks = {}
	for x=-r, r do
		for y=-r, r do
			chunks[#chunks + 1] = {x, y}
		end
	end
	surface.regenerate_entity(entities, chunks)
	for _, ent in pairs(surface.find_entities_filtered{name="electric-mining-drill"}) do
		surface.create_entity{name=ent.name, position=ent.position, direction=ent.direction,force=ent.force}
		ent.destroy()
	end
end

function reset_surfaces()
	local surfaces = {}
	for _, player in pairs(game.players) do
		if not surfaces[player.surface.index] then
			game.print(player.surface.name)
			reset_resources(player.surface)
			surfaces[player.surface.index] = true
		end
	end
end

function shift_surface(player, step)
	if #game.surfaces <= 1 or step == 0 then return end

	local sign = (step > 0) and 1 or -1
	local index = player.surface.index
	for _=1, step / sign do
		repeat
			index = (index + step - 1) % #game.surfaces + 1
		until game.surfaces[index] and game.surfaces[index].valid and not (global.shift_excluded and global.shift_excluded[index])
	end
	player.teleport(player.position, game.surfaces[index])
end

function importbp(blueprint_stack, data)
	for index, entity in pairs(data.entities or {}) do
		if not entity.entity_number then entity.entity_number = index end
	end
	blueprint_stack.set_blueprint_entities(data.entities or {})
	blueprint_stack.label = data.name
	blueprint_stack.blueprint_icons = data.icons
	blueprint_stack.set_blueprint_tiles(data.tiles or {})
end



-- Inputs
---------

script.on_event("more-zoom", function (event)
	if not global.zoom then global.zoom = {} end
	if not global.zoom[event.player_index] then global.zoom[event.player_index] = 0 end
	if global.zoom[event.player_index] < 20 then
		global.zoom[event.player_index] = global.zoom[event.player_index] + 1
	end
	game.players[event.player_index].zoom = (1/2)^(global.zoom[event.player_index] / 3)
end)
script.on_event("less-zoom", function (event)
	if not global.zoom then global.zoom = {} end
	if not global.zoom[event.player_index] then global.zoom[event.player_index] = 0 end
	if global.zoom[event.player_index] > -20 then
		global.zoom[event.player_index] = global.zoom[event.player_index] - 1
	end
	game.players[event.player_index].zoom = (1/2)^(global.zoom[event.player_index] / 3)
end)


script.on_event("surface-up", function (event)
	shift_surface(game.players[event.player_index], 1)
end)
script.on_event("surface-down", function (event)
	shift_surface(game.players[event.player_index], -1)
end)

script.on_event("reset-resources", reset_surfaces)

-- Events
-- script.on_init(function()
-- end)



-- API

--[[
	 /c remote.call("tasplanner", "calc", {
		 "rocket-silo",
		 "concrete",
		 "electric-energy-accumulators-1",
		 "solar-energy",
		 "effect-transmission",
		 "fluid-handling",
		 "logistics-2",
		 "automobilism",
		 "electric-engine",
	})
--]]

commands.add_command("importbp", "Import blueprint. Use at own risk!", function(event)
	local player = game.players[event.player_index]
	if not player.clean_cursor() then game.print("Inventory full. Could not insert imported blueprint!"); return end
	game.player.cursor_stack.set_stack{name="blueprint"}
	local f = loadstring(event.parameter)
	local data = f()
	if (not type(data) == "table") or (not data.entities and not data.tiles) then
		game.print("Invalid argument for importbp.")
	else 
		importbp(game.player.cursor_stack, data)
	end
end)

commands.add_command("newsurface", "Create new surface. ", function ()
	local surface = game.create_surface("surface " .. math.random(2^32), game.surfaces.nauvis.map_gen_settings)
	surface.always_day = true
end)

commands.add_command("resetsurfaces", "Regenerate resources on active surfaces.", reset_surfaces)

remote.add_interface("tasplanner", {
	calc = calculate_technologies,
	--[[
	Example:
	/c remote.call("tasplanner", "importbp", game.player, <data>)
	--]]
	importbp = function(player, data)
		if not player.clean_cursor() then game.print("Inventory full. Could not insert imported blueprint!"); return end
		game.player.cursor_stack.set_stack{name="blueprint"}
		importbp(game.player.cursor_stack, data)
	end,
})

local exportbp_help = "Usage: /exportbp <name>. "
	.. "Export the blueprint that is currently in the hand of the player to script-output/Blueprints/<name>. "
	.. "If no argument is passed, use the name of the blueprint item."
commands.add_command("exportbp", exportbp_help, function(args)
	local param_string = args.parameter
	local player = game.players[args.player_index]
	local log = "Blueprint export. "
	local name

	if not param_string or param_string == "" then
		log = log .. "No name argument passed. "
	else
		name = param_string
	end

	local blueprint = player.cursor_stack
	if not blueprint or not blueprint.valid_for_read or not blueprint.is_blueprint_setup() then
		log = log .. "Cursor does not hold valid blueprint! Export failed! "
		game.print(log)
		return
	end

	if not name then
		name = blueprint.label
		if not name then
			log = log .. "No blueprint label set! Export failed! "
			game.print(log)
			return
		else
			log = log .. "Using blueprint label. "
		end
	end
	
	anchor_entity = "programmable-speaker"
	
	local anchor
	local blueprint_entities = blueprint.get_blueprint_entities()
	
	for i,entity in pairs(blueprint_entities) do
		if entity.name == anchor_entity then
			anchor = entity.position
			table.remove(blueprint_entities, i)
			
			break
		end
	end
	
	if not anchor then
		log = log .. "No anchor set! Export failed! "
		game.print(log)
		return
	end
	
	local blueprint_table =
	{
		entities = blueprint_entities,
		name = name,
		tiles = blueprint.get_blueprint_tiles(),
		icons = blueprint.blueprint_icons,
		anchor = anchor,
	}

	local output = serpent.dump(blueprint_table)

	local filename = "Blueprints/" .. name .. ".lua"

	game.write_file(filename, output, false, player.index)
	game.print(log .. "Export successful! " .. name)
end)
