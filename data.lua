-- Inputs
data:extend{
	{
		type = "custom-input",
		name = "more-zoom",
		key_sequence = "Mouse button 4",
	},
	{
		type = "custom-input",
		name = "less-zoom",
		key_sequence = "Mouse button 5",
	},
	{
		type = "custom-input",
		name = "surface-up",
		key_sequence = "CONTROL + Mouse button 4",
		consuming = "all"
	},
	{
		type = "custom-input",
		name = "surface-down",
		key_sequence = "CONTROL + Mouse button 5",
		consuming = "all"
	},
	{
		type = "custom-input",
		name = "reset-resources",
		key_sequence = "CONTROL + G"
	},
} 

